#!/usr/bin/python3

def pizzaparse( pizzastr ):
    splitted = pizzastr.split(",")

    if len(splitted) != 3:
        return None;

    try:
        size=int(splitted[1])
        price=int(splitted[2])
    except ValueError:
        return None;
    return [splitted[0],size,price]


import argparse
import math
from operator import itemgetter

parser = argparse.ArgumentParser(description='Calculate pizza value')
parser.add_argument('pizzas', metavar='pizza_values', type=str, nargs='+',
                    help='pizza sizes and prices in format size=price')

args = parser.parse_args()

pizzas = []
ratios = []

for i in args.pizzas:
    pizzas.append(pizzaparse(i))

pizzas = [x for x in pizzas if x is not None]

for i in pizzas:
    i.append(math.pi*((i[1]*i[1])/4))
    i.append(10000*(i[2]/i[3]))

sortedpizzas = sorted(pizzas, key=itemgetter(4))

for i in sortedpizzas:
    print(i)
